import Data.List

s = ["hello", "stone", "could", "parts", "local", "strap", "notes", "traps", "cloud"]

isSame :: (String, String) -> (String, String) -> Bool
isSame a b = fst a == fst b

cluster :: [String] -> [[(String, String)]]
cluster xs = groupBy isSame $ sort [(sort x, x) | x <- xs]

isAnagram :: [a] -> Bool
isAnagram x = (length x) > 1

anagramClusters :: [[(String, String)]] -> [[(String, String)]]
anagramClusters xs = filter isAnagram xs

pickAnagrams :: [[(String, String)]] -> [[String]]
pickAnagrams as = [[snd y | y <- x] | x <- as]

main = do
    print $ pickAnagrams $ anagramClusters $ cluster s

