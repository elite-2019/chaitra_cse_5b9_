module Main where

divides :: Int -> Int -> Bool
divides n f = rem n f == 0

ldf :: Int -> Int -> Int
ldf n k | k * k > n   = n
ldf n k | divides n k = k
ldf n k | otherwise   = ldf n (k + 1)

ldp :: Int -> Int -> Int
ldp n (p:ps) | p * p > n   = n
ldp n (p:ps) | divides n p = p
ldp n (p:ps) | otherwise   = ldp n ps 

ld :: Int -> Int
ld n = ldf n primes

isPrime :: Int -> Bool
isPrime n = ld n == n

factorize :: Int -> [Int]
factorize n | ld n == n = [n]
factorize n | otherwise = [f] ++ factorize (n `div` f) where f = ld n

primes :: [Int]
primes = [2] ++ filter isPrime [3,5..]

main :: IO ()
main = do
    putStrLn "hello world"


