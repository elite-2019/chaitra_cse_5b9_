import Data.List
import Data.Char

removeElements :: String -> [Char] -> String
removeElements s l = snd (partition (`elem` l) s)

upperCase :: String -> String
upperCase = map toUpper

main = do
    print $ upperCase $ removeElements "Hello World" ['a', 'e', 'i', 'o', 'u']


