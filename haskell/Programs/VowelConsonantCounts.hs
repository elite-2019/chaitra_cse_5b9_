import Data.Char
import Data.List

countGroup :: String -> String -> Int
countGroup g s = length [ch | ch <- s , elem (toLower ch) g]

countVowels = countGroup "aeiou"
countConsos = countGroup "bcdfghjklmnpqrstvwxyz"


vs s = concat $ map show $ map countVowels $ words s   
cs s = concat $ map show $ map countConsos $ words s

flap s = vs s ++ " " ++ cs s 

main = do
       print $ flap "Parrot egg not green"

