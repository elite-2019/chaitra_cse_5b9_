nextCollatz :: Int -> Int
nextCollatz n = if even n then (quot n 2) else (3 * n + 1)


collatzSequence :: Int -> [Int]
collatzSequence 2 = [4, 2, 1]
collatzSequence n = n : collatzSequence (nextCollatz n)

main = do
    input1 <- getLine
    let x = (read input1 :: Int)
    print $ collatzSequence x


