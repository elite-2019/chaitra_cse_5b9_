--Create a function perms that takes a list with no duplicate elements and generates all permutations of that list.

perms :: Eq a => [a] -> [[a]]
perms [] = [[]]
perms xs = [ i:j | i <- xs, j <- perms $ delete i xs ]
