kap' :: Integer -> [Integer] -> [Integer]
kap' n ns
 | ns == [] = kap' n [n]
 | n' `elem` ns  = ns
 | otherwise = k
  where
    k    = kap' n'  (ns ++ [n'])         
    digs = toDigs n 
    n1'  = fromDigs . sortDesc $ digs
    n2'  = fromDigs . sort     $ digs
    n'   = abs (n1' - n2')
