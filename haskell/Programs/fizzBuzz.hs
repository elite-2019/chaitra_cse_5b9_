import Data.Maybe
import Data.Monoid

fizz :: [Maybe String]
fizz = cycle [Nothing , Nothing , Just "FIZZ"]

buzz :: [Maybe String]
buzz = cycle [Nothing , Nothing , Nothing , Nothing ,  Just "Buzz"]

fizzbuzz :: [Maybe String]
fizzbuzz = zipWith (<>) fizz buzz

num :: [String]
num = show <$> [1..]

finalFizzBuzz :: [String]
finalFizzBuzz = zipWith fromMaybe num fizzbuzz



main = do
      print $ take 100 finalFizzBuzz 

