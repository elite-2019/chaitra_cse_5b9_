xform :: [Int] -> [Bool]
xform q = [odd x | x <- q]

distributable :: [Bool] -> Bool
distributable q = even $ length $ filter id q

distribute :: [Int] -> Int
distribute q = if not (distributable q')
               then -1
               else distribute' q'
                    where q'= xform q

distribute' :: [Bool] -> Int
distribute' qq@(q:qs)    | length qq == 0  = 0
distribute' qq@(q:qs)    | notElem True qq = 0
distribute' qq@(q:qs)    | not q           = distribute' qs
distribute' qq@(q:q':qs) | otherwise       = 2 + (distribute'$ (not q'):qs)
