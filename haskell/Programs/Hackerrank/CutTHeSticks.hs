import Control.Applicative
import Data.List

sticks :: [Int] -> [Int]
sticks = sticksSorted . sort
  where sticksSorted [] = []
        sticksSorted numbers@(x:xs) = let l = length numbers
                                          newNumbers = dropEqual x xs
                                      in (l : sticksSorted newNumbers)
          where dropEqual _ [] = []
                dropEqual y ls@(z:zs) | z == y = dropEqual y zs
                                      | otherwise = ls

getInts :: IO [Int]
getInts = (map read . words) <$> getLine

main :: IO ()
main = do
  _ <- getLine
  numbers <- getInts
  mapM_ print $ sticks numbers
