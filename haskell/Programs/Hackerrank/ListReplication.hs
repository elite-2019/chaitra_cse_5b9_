module Main where

import Control.Monad
import Data.Array
import Data.Bits
import Data.List
import Data.List.Split
import Data.Set
import Debug.Trace
import System.Environment
import System.IO
import System.IO.Unsafe


import Control.Monad (replicateM_)

hello_worlds :: Int -> IO ()
hello_worlds n = replicateM_ n $ putStrLn "Hello World"

main :: IO()
main = do
    n <- readLn :: IO Int
    hello_worlds n 
