import Data.List

s = ["hello", "stone", "could", "parts", "local", "strap", "notes", "traps", "cloud"]

isSame :: String -> String -> Bool
isSame a b = (sort a) == (sort b)

isAnagram :: [a] -> Bool
isAnagram a = (length a) > 1

anagramClusters :: [String] -> [[String]]
anagramClusters s = filter isAnagram $ groupBy isSame $ sortOn sort s

main = do
    print $ anagramClusters s

