def max_score(test_list):
  res = max(test_list, key = lambda i : i[1])[1]
  return res

def score(test_list):
  score_list = []
  for i in test_list:
    if max_score(test_list) == i[1]:
      score_list.append(i)
  return score_list

def score_count(score_list,dscore):
  player1_score = []
  player2_score = [] 
  for i in score_list :
    if 'player1' in i[0]:
      player1_score.append(dscore / len(score_list))
    if 'player2' in i[0]:
      player2_score.append(dscore / len(score_list))
  return player1_score,player2_score

