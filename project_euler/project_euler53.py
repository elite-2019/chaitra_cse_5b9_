#Problem 53:
#There are exactly ten ways of selecting three from five, 12345:
#123, 124, 125, 134, 135, 145, 234, 235, 245, and 345
#In combinatorics, we use the notation, 5C3 = 10.
#In general,
#nCr = n! / (r!(n−r)!)
#where r ≤ n, n! = n×(n−1)×...×3×2×1, and 0! = 1.
#It is not until n = 23, that a value exceeds one-million: 23C10 = 1144066.
#How many, not necessarily distinct, values of  nCr, for 1 ≤ n ≤ 100, are
#greater than one-million?

import math

def count(minimum_number = 23,maximum_number=100,minimumCombinations = 1000000):
    number = 0
    for n in range(minimum_number,maximum_number + 1):
        for r in range(1, n // 2 + 1):
            combination = math.factorial(n) / (math.factorial(r) * math.factorial(n - r))
            if combination > minimumCombinations :
                if n == 2 * r:
                    number += 1
                else:
                    number += 2
    return number

print(count())

#output : 4075
