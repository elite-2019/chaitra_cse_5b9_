#145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145. Find the sum of all numbers which are equal to the sum of the factorial of their digits. Note: as 1! = 1 and 2! = 2 are not sums they are not included.

from math import factorial
l = lambda n: reduce(lambda x, y: int(x) + factorial(int(y)), 
                     [d for d in "0" + str(n)])
print(reduce(lambda x,y: x+y, 
             filter(lambda x: x == l(x), 
                    xrange(3, 100000))))
