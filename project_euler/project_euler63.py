#project euler 63

def all_numbers(digit):
    return [i for i in range(100000) if len(str(i)) == digit]
def power(digit):
    return { digit: [i for i in all_numbers(digit) for y in range(0, 10)  if y ** digit == i]}
for digit in range(2, 5):
    print(power(digit))
