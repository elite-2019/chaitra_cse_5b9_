def powerful_digit_count():
    n_digit_positiveInteger = sum(1
            for i in range (1,10) 
            for j in range (1,22)
            if len (str(i ** j)) == j)
    return str(n_digit_positiveInteger)

print(powerful_digit_count)
