#project euler 655
def reverse(s): 
    return s[::-1] 
  
def is_palindrome(num):  
    rev = reverse(str(num))
    if(str(num) == str(rev)):
        return True
    return False
def palindrome(LIMIT):
    return [i for i in range(1, LIMIT + 1) if is_palindrome(i)]
result = [num for num in palindrome(100000) if num % 109 == 0]
print(result)
