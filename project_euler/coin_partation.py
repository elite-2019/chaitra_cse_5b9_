# list of generalized pentagonal numbers for generating function
pentagonal_numbers = sum([[i*(3*i - 1)/2, i*(3*i - 1)/2 + i] for i in range(1, 250)], [])

p, sgn, n, m  = [1], [1, 1, -1, -1], 0, 1e6

while p[n]>0:    # expand generating function to calculate p(n)
    n += 1
    px, i = 0, 0
    while pentagonal_numbers[i] <= n:
        px += p[n - pentagonal_numbers[i]] * sgn[i%4]
        i += 1
    p.append(px % m)

print "Project Euler 78 Solution.Coin partions  =", n



#A partition of an integer, n, is one way of describing how many ways the sum of positive integers, ≤ n, can be added together to equal n, regardless of order. The function p(n) is used to denote the number of partitions for n. Below we show our 5 “coins” as addends to evaluate 7 partitions, that is p(5)=7.
#5 = 5
#= 4+1
#= 3+2
#= 3+1+1
#= 2+2+1
#= 2+1+1+1
#= 1+1+1+1+1
#We use a generating function to create the series until we find the required n.
#The generating function requires at most 500 so-called generalized pentagonal numbers, given by n(3n – 1)/2 with 0, ± 1, ± 2, ± 3…, the first few of which are 0, 1, 2, 5, 7, 12, 15, 22, 26, 35, …