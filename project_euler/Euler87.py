from sympy import isprime,primerange

def generataPrimePowerTriples(LIMIT):
    primepowertriples = {0}
    for i in range(2, 5):
        temp = set()
        for primes in primerange(2,LIMIT):
            power = primes ** i
            if power > LIMIT:
                break
            for x in primepowertriples:
                if x + power <= LIMIT:
                    temp.add(x + power)
        primepowertriples = temp
    return len(primepowertriples)
    
    
print(generataPrimePowerTriples(50000000))