#Abundant numbers

from math import sqrt
from sympy import isprime


def lo_divisors(num):
    return [i for i in range(2,int(sqrt(num))+1) if num % i == 0]
    
def high_divisors(num):
    loDivisors = lo_divisors(num)
    return [num//i for i in loDivisors]
    
def max_prime_divisor(num):
    all_divisors = list(set([1] + lo_divisors(13195) + high_divisors(13195)))
    return max([i for i in all_divisors if isprime(i)])
    
max_prime_divisor(13195)