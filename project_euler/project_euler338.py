# project euler 388
from itertools import product as prod

cartesian_product = list(prod(range(6), repeat = 3))

print(cartesian_product)
print(len(cartesian_product))
