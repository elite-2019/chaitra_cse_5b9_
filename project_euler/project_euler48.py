#problem : 48 -- The series, 11 + 22 + 33 + ... + 1010 = 10405071317.

#Find the last ten digits of the series, 11 + 22 + 33 + ... + 10001000.


LIMIT = 1000    

d = 10**10

#s = sum(pow(n, n, d) for n in range(1, LIMIT + 1)) 
s = sum(pow(n, n) for n in range(1, LIMIT + 1))
print ("LARGEST TEN DIGIT OF SERIES = %d" % (s % d))        

#Output : LARGEST TEN DIGIT OF SERIES = 9110846700
