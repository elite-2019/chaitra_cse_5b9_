'''
Problem 65:
The square root of 2 can be written as an infinite continued fraction.
The infinite continued fraction can be written, √2 = [1;(2)], (2) indicates
that 2 repeats ad infinitum. In a similar way, √23 = [4;(1,3,1,8)].
It turns out that the sequence of partial values of continued fractions for
square roots provide the best rational approximations. Let us consider the
convergents for √2.
Hence the sequence of the first ten convergents for √2 are:
1, 3/2, 7/5, 17/12, 41/29, 99/70, 239/169, 577/408, 1393/985, 3363/2378, ...
What is most surprising is that the important mathematical constant,
e = [2; 1,2,1, 1,4,1, 1,6,1 , ... , 1,2k,1, ...].
The first ten terms in the sequence of convergents for e are:
2, 3, 8/3, 11/4, 19/7, 87/32, 106/39, 193/71, 1264/465, 1457/536, ...
The sum of digits in the numerator of the 10th convergent is 1+4+5+7=17.
Find the sum of digits in the numerator of the 100th convergent of the
continued fraction for e.
'''
#Analysis : The numerator follows a predictable pattern (sequence) and we are exploiting that to solve this problem. There is a multiplier, m, that follows the sequence mentioned in the problem statement [2; 1,2,1, 1,4,1, 1,6,1 , … , 1,2k,1, …] starting with the third term, m2.
#The numerator follows the sequence (1, 2, 3, 8, 11, 19, 87, …) as defined below: n_i = m_i * n_{i-1} + n_{i-2}, where\:n_0=1\:and\:n_1=2 So, n6 = 4*19 + 11 or 87 and n10 = 1*193 + 1264 or 1457

# There's a recursive formula about the numerator
# n(k) = c(k) * n(k-1) + n(k-2)

LIMIT = 100
n0, n1, = 1, 2

for i in xrange(2, LIMIT+1): 
    n0, n1 = n1, n0 + n1 * (1 if i%3 else 2 * i//3)

print "SUM OF DIGITS =", sum(map(int, str(n1)))

#Output: SUM OF DIGITS = 272