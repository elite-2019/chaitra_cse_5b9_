#Project Euler Problem 100

#If a box contains twenty-one coloured discs, composed of fifteen blue discs and six red discs, and two discs were taken at random, it can be seen that the probability of taking two blue discs, P(BB) = (15/21)×(14/20) = 1/2.

#The next such arrangement, for which there is exactly 50% chance of taking two blue discs at random, is a box containing eighty-five blue discs and thirty-five red discs.

#By finding the first arrangement to contain over 1012 = 1,000,000,000,000 discs in total, determine the number of blue discs that the box would contain.

# Simple analysis
# denote blue disc number as b, total disc number as n
# we have:
# (b / n) * ((b - 1) / (n - 1)) = 1 / 2
# so n^2 - 2b^2 - n + 2b = 0
# since n and b must be integer, this is a quadratic Diophantine Equation

# n(k+1) = 3 * n(k) + 4 * b(k) - 3
# b(k+1) = 2 * n(k) + 3 * b(k) - 2

#From here we have two equations that can calculate b and n for each term in the series. Instead of starting at b=1, n=1 we start with the values in the problem description to save a few iterations; b=85, n=120. We continue this series until n>1012.

#The equations are: blue disks = 3b + 2n – 2; n disks = 4b + 3n – 3 


blue_discs = 3
Tot_no_discs = 4
LIMIT = 1000

while Tot_no_discs <= LIMIT:

    blue_discs,Tot_no_dicsc = 3*blue_discs + 2*Tot_no_discs - 2, 4*blue_discs + 3*Tot_no_discs - 3



print( "Total disks =", LIMIT)

print("Number of Blue disks, number of total disks =", blue_discs, Tot_no_discs)


#Output :
#Total disks = 1000
#Number of Blue disks, number of total disks = 2871 4060
