#Problem 57:

#It is possible to show that the square root of two can be expressed as an
#infinite continued fraction.
#√ 2 = 1 + 1/(2 + 1/(2 + 1/(2 + ... ))) = 1.414213...
#By expanding this for the first four iterations, we get:
#1 + 1/2 = 3/2 = 1.5
#1 + 1/(2 + 1/2) = 7/5 = 1.4
#1 + 1/(2 + 1/(2 + 1/2)) = 17/12 = 1.41666...
#1 + 1/(2 + 1/(2 + 1/(2 + 1/2))) = 41/29 = 1.41379...
#The next three expansions are 99/70, 239/169, and 577/408, but the eighth
#expansion, 1393/985, is the first example where the number of digits in the
#numerator exceeds the number of digits in the denominator.
#In the first one-thousand expansions, how many fractions contain a numerator
#with more digits than denominator?


from math import log10Problem 57:
    
LIMIT = 1000
Numerator = 3
Denominator = 2
count = 0, n, d, c = 1000, 3, 2, 0

for x in range(2, LIMIT+1):
    Numerator, Denominator = Numerator + 2*Denominator, Numerator + Denominator
    if int(log10(Numerator)) > int(log10(Denominator)): count += 1

print ("Numerator more digits than denominator =", count)

#output : Numerator more digits than denominator = 153
