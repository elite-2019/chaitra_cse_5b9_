#project euler 544
from itertools import product as prod
import math
row = 2
column = 2
color = 3
def colored_grids():
    return list(prod(range(color), repeat = row*column))
def no_same_color_adjacent_grids(i: tuple) -> bool:
    for j in range(len(i)):
        if i[j] == i[j+1]:
            return False
        while(j * j <= row*column):
            if (i[j] == i[j + column]):
                return False
    return True
def all_different_grids():
    return [i for i in colored_grids() if no_same_color_adjacent_grids(i)]

#print(colored_grids())
#print(no_same_color_adjacent_grids((1,2,3,4)))
#print(all_different_grids())
