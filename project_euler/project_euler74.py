import math
def digital_factorial_chains():
    LIMIT = 10**6
    return str(sum(1 for i in range(LIMIT) if get_chain_length(i) == 60))
def get_chain_length(n):
    seen = set()
    while True:
        seen.add(n)
        n = factorialize(n)
        if n in seen:
            return len(seen)
def factorialize(n):
    result = 0
    while n != 0:
        result += FACTORIAL[n % 10]
        n //= 10
    return result

FACTORIAL = [math.factorial(i) for i in range(10)]
print(digital_factorial_chains)
