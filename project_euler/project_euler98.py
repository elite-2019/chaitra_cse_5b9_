# the top anagram for a set of digits 4 through 13:

from collections import defaultdict

for digits in range(3,14):
	d = defaultdict(list)
	for i in range(int((10**(digits-1))**0.5), int((10**digits)**0.5)+1):
		j = ''.join(sorted(str(i*i)))
		d[j].append(i)
	max_key = max(d, key= lambda x: len(set(d[x])))
	print(d[max_key][-1]**2)

#Output :

#961
#9216
#96100
#501264
#9610000
#73462041
#923187456
#9814072356
#98310467025
#985203145476
#9831140766225

#problem : 98
#By replacing each of the letters in the word CARE with 1, 2, 9, and 6 respectively, we form a square number: 1296 = 362. What is remarkable is that, by using the same digital substitutions, the anagram, RACE, also forms a square number: 9216 = 962. We shall call CARE (and RACE) a square anagram word pair and specify further that leading zeroes are not permitted, neither may a different letter have the same digital value as another letter.
#code pending