from Euler import is_prime, is_perm

n, f = 1487, 1 	# start search with prime from description
while True:
    n += 3-f    # generates prime candidates faster than checking odd numbers
    f = -f
    b, c = n+3330, n+6660
    if is_prime(n) and is_prime(b) and is_prime(c) \
        and is_perm(n,b) and is_perm(b,c): break

print "Project Euler 49 Solution =", str(n)+str(b)+str(c), n, b, c