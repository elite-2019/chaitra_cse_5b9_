#"""
#Starting in the top left corner of a 2×2 grid, and only being able to move to
#the right and down, there are exactly 6 routes to the bottom right corner.
#How many such routes are there through a 20×20 grid?
#"""
#Using the binomial coefficient
#It could also be described as how many distinct ways can you shuffle the characters in the string “RRRRDDDD”.

#Continuing our thinking from above we realize that determining the number of contiguous routes for a square grid (n × n) is the central binomial coefficient or the center number in the 2nth row of Pascal's triangle. The rows start their numbering at 0.
#Pascal's Triangle
 #{2n \choose n} = \frac{(2n)!}{n!~\times~n!} 
#The formula in general for any rectangular grid (n × m) using the notation for the binomial coefficient is:

 #{n+m \choose n} = \frac{(n~+~m)!}{n!~\times~m!} 

f = lambda n: reduce(lambda x,y:x*y, xrange(1,n+1), 1)
n, m = 20, 20

print "Routes through a", n, "x", m, "grid", f(n+m) // f(n) // f(m)

#Outputs : Routes through a 20 x 20 grid 137846528820