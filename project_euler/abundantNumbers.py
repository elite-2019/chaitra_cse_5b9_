import math

from itertools import product as prod 

ABUNDANT_LIMIT = 28123

def lo_divisors(n):
    LIMIT - int(math.sqrt(n))
    return [f for f in range(2, LIMIT+1) if n%f == 0]
    
def hi_divisors(n):
    return [n//f for f in lo_divisors(n)]
    
def all_divisors(n):
    return set([1] + lo_divisors(n) + hi_livisors(n))

def max_prime_divisor(n):
    return max([i for i in divisors if isprime(i)])

max_prime_divisor(13195)




#The abundant numbers are positive integers n for which the sum of divisors of n exceeds  2n .
#In order to determine if a number is abundant, you must first find all of the number's proper factors.c
# While factors are numbers that you multiply together to get another number, proper factors are all the factors of the number except the number itself.
