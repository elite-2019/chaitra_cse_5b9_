digits = lambda n, p: set(str(n*p))
d = lambda n: all(digits(n, 1) ==  digits(n, x) for x in range(2, 7))
print(filter(d, range(0, 1000000)))
