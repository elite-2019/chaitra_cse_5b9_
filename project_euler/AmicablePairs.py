#return list of tuples where each tuple is Amicable Pair
def sum_factors(n):
	list1 = []
	for i in range(1, n):
		if(n % i == 0):
			list1.append(i)
	return sum(l1)

def amicablePairs(n):
	l = []
	for i in range(1, n):
            x = sum_factors(i)
            if(i != x):
                if((x, i) not in l):
                    if(sum_factors(x) == i):
                        l.append((i, x))
	return l

print(amicablePairs(10000))


