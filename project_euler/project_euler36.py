#The decimal number, 585 = 10010010012 (binary), is palindromic in both bases. Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
palindrom = lambda x: str(x) == str(x)[::-1] and str(bin(x))[2:] == str(bin(x))[2:][::-1]
pandindromic_sum = lambda top: reduce(lambda x,y: x+y, 
                       filter(palindrom, range(0,top+1)))
