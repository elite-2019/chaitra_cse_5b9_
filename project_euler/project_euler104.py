def fibGen():
    a,b = 1,1
    while True:
        a,b = b,a+b
        yield a

k = 0
fibG = fibGen()
while True:
    k += 1
    x = str(fibG.next())
    if (set(x[-9:]) == set("123456789")):
        print(x) #debugging print statement
        if(set(x[:9]) == set("123456789")):
            break

print (k)
