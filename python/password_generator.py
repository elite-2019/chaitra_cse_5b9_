
# generate a password with length "passlen" with no duplicate characters in the password

import string
import random

def pw_gen(size = 8, chars=string.ascii_letters + string.digits + string.punctuation):
	return (''.join(random.choice(chars) for _ in range(size)))

print(pw_gen(int(input('How many characters you need in your password?: '))))

#Output
#How many characters you need in your password?: 5
#-uP]t

