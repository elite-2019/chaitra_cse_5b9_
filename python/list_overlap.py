#Question:
#Take two lists, say for example these two:
#
#	a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
#	b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
#and write a program that returns a list that contains only the elements that are common between the lists (without duplicates). Make sure your program works on two lists of different sizes.

a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
c = []

def overlap(val1,val2,val3):
    for num in val1:
        if num in val2:
            if num not in val3:
                val3.append(num)


overlap(a,b,c)
print(c)

#Output:[1, 2, 3, 5, 8, 13]

