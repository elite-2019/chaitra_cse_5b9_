string = "Ronaldo is betther than Messi"

print(string.find("Ronaldo",5))
print(string.find("football"))
print(string.find("Messi",5,100))


#To check whether a string s1 contains another string s2, you can use the method find(). This method is slightly more powerful than the Python "in" keyword because it also returns the index of the searched substring (instead of returning only a Boolean value).

#If the substring does not exist, the find() method returns the index -1. You have to be careful to handle this return value properly.

#The puzzle shows how you can specify the start and stop indices to limit the search to a certain (index) range.

#Output: -1 -1 23 
#The find() method returns the index of first occurrence of the substring (if found). If not found, it returns -1.
