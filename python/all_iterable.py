def all(iterable):
    for element in iterable:
        if not element:
            return False
    return True

print(all([])) #=> True  # Because an empty iterable has no non-truthy elements
print(all([6, 7])) #=> True
print(all([6, 7, None])) #=> False  # Because it has None
print(all([0, 6, 7])) #=> False  # Because it has zero
print(all([9, 8, [1, 2]])) #=> True
print(all([9, 8, []])) #=> False  # Because it has []
print(all([9, 8, [1, 2, []]])) #=> True
print(all([9, 8, {}])) #=> False  # Because it has {}
print(all([9, 8, {'engine': 'Gcloud'}])) #=> True


#Output : 
#True
#True
#False
#False
#True
#False
#True
#False
#True

