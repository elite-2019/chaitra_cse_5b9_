# Python Puzzle
def len_(x):
    length = len(x)
    # print everything in same row
    print(length, end='')
    return length

items = [3.14, 'moon', { }]
i = 0

while i < len_(items):
    i += 1


#The answer is length of the list times plus one when the condition becomes False. Since we have three elements in the list, the function in the loop condition is called four times.
#With the end='' parameter you can set the symbol to be printed after the text. By default it is a line break. If you set end to empty string all the outputs appear in the same row.
#The length of the list is three, _len() gets called four times and each time we print the list's length in the same row on the console. Thus the output is 3333.
