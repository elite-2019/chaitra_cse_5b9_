def cut_stick(stick, cut_size):
    return stick - cut_size if stick > 0 else stick

def read_ints():
    return map(int,input().strip().split())

def read_int():
    return int(input().strip())

def main(sticks):    
    while not all(x == 0 for x in sticks):
        min_cut_size = min(i for i in sticks if i > 0)
        sticks_cut = len(filter(lambda x: x >= min_cut_size, sticks))
        sticks = map(lambda x: cut_stick(x, min_cut_size), sticks)
        print(sticks_cut)

if __name__ == '__main__':
    _ = read_int() 
    k = read_ints()
    main(k)
