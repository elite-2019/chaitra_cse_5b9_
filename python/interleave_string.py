#how to interleave two strings in Python (with potentially variable length).

from itertools import zip_longest

def interleave_strings(string1, string2):
    tuples = zip_longest(string1, string2, fillvalue='')
    string_list = [''.join(item) for item in tuples]
    return ''.join(string_list)

string_1 = "AAA"
string_2 = "BBBBB"
print(interleave_strings(string_1,string_2))

#First, we convert the string s2 to a list of characters using the list(…) function. This is the basis of our solution.

#Second, we insert the characters of the string s1 at positions 0, 2, 4, … by iterating over all indices i and characters c of the first string s1. Now we insert the characters into every other position of the list.

#output:ABABABBB