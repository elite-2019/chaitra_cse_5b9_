words = ['ape', 'banana', 'cat', 'bird']

b_words = [w for w in words if w.startswith('b')]

print(len(b_words))

#Output : 2

#The list comprehension iterates over the given list of words and puts all words that start with a 'b' in a new list. Since there are two words that start with a 'b' the length of the new list is 2.


