(ns my.combinatorics
  (:require [clojure.math.combinatorics :refer [permutations]]))

(permutations [1 2 3])
