(ns anagram
  (:require [clojure.string :as str]))

(defn find-words-of-same-length [value collection]
  (remove nil? (map (fn [word] (cond
                                (true? (= (count value) (count word))) word)) collection)))

(defn find-matching-words [value collection]
  (remove nil? (map (fn [word] (cond
                                (false? (= (str/upper-case word) (str/upper-case value))) word)) collection)))

(defn find-words-with-matching-letters [value collection]
    (remove nil? (map (fn [word] (cond
                                  (true? (= (frequencies (str/upper-case value)) (frequencies (str/upper-case word)))) word)) collection)))

(defn anagrams-for [value collection]
  (into []  (find-words-with-matching-letters value (find-matching-words value (find-words-of-same-length value collection)))))
