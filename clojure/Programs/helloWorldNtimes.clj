(defn hello_word_n_times[n]
    (if (not= n 0) 
        (do
            (println "Hello World")
            (recur (dec n)))))
