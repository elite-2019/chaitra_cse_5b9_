(fn [coll]
  (reverse
   (reduce #(cons %2 (cons %2 %1)) '() coll)))
