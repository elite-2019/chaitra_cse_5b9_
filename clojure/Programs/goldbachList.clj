(defn sqrt
    "Calculate the floor of the square root of n"
    [n]
    (nth (exact-integer-sqrt n) 0))

(defn divides?
    "Determine if k divides n"
    [k n]
    (zero? (mod n k)))

(defn prime?
    "Determine if n is prime"
    [n]
    (if (= 1 n)
        false
        (->>
            (sqrt n)
            (inc)
            (range 2)
            (filter #(divides? % n))
            (empty?))))

(defn gcd
    "Calculate the greatest commond divisor of two integers using Eclid's algorithm"
    [m n]
    (if (< m n)
        (recur n m)
    (if (zero? n)
        m
        (recur n (mod m n)))))

; Problem 33
(def coprime?
    "Determine if two numbers are coprime"
    (comp (partial = 1) gcd))


(defn totient
    "Caclulate Euler's totient function"
    [n]
    (->> n
        (range 1)
        (filter (partial coprime? n))
        (count)))

(defn prime-factors
    "Calculate the prime factors of a number, without multiplicity"
    [n]
    (->> n
        (inc)
        (range 2)
        (filter #(and (divides? % n) (prime? %)))))

(defn multiplicity
    "Calculate the multiplicity of k as a divisor of n"
    [k n]
    (->>
        (range)
        (drop-while #(divides? (pow k (inc %)) n))
        (first)))


(defn prime-factorization
    "Calculate the prime factorization of a number"
    [n]
    (->>
        (prime-factors n)
        (map #(vector % (multiplicity % n)))))



(defn totient'
    "Efficient calculation of Euler's totient function"
    [n]
    (->>
        (prime-factorization n)
        (map (fn [[p k]] (int (* (dec p) (pow p (dec k))))))
        (reduce *)))


(defn primes
    "List the primes in a given range"
    [low high]
    (filter prime? (range low (inc high))))


(defn goldbach
    "Find two primes that sum to the given (even) integer"
    [n]
    (->>
        (quot n 2)
        (primes 1)
        (drop-while #(not (prime? (- n %))))
        (first)
        (#(vector % (- n %)))))


(defn goldbach-list
    "List Goldbach decompositions of even numbers in a given range"
    [low high]
    (->>
        (range low (inc high))
        (filter even?)
        (map goldbach)))
