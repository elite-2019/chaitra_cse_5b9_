(defn max-sub-array-reduction [A]
  (letfn [(find-max-sub-array [[max-ending-here max-so-far] x]
            [(max x (+ max-ending-here x)) (max max-so-far
                                                max-ending-here)])]
(second (reduce find-max-sub-array [0 0] A))))
