(defn count-vowels [s] (count (filter #{\a \e \i \0 \u \A \E \I \O \U} (seq s))))
