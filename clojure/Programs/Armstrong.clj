(defn digits [n]
  (map #(- (int %) (int \0))
       (str n))
  )
(defn pow[k n]
  (int (Math/pow k n))
  )
(defn armstrong? [n]
  (let [d (digits n)
         k (count d)]
    (= n
       (reduce +(map #(pow % k) d)))
    )
  )


