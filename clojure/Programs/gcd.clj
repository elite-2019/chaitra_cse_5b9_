;; Tail recursion
;; The following function calculates the greatest common denominator of two numbers:
(defn gcd [x y]
  (cond
    (> x y) (gcd (- x y) y)
    (< x y) (gcd x (- y x))
    :else x))
