(fn [coll] (map first (partition-by identity coll)))
