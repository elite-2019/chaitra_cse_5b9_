(fn [coll]
  (reduce (fn [x y] (inc x)) 0 coll))
