(defn fibs []
  (map first (iterate (fn [[a b]] [b (+ a b)]) [1 2]))
  )

(defn even_Fibs[]
  (reduce + (filter even? (take-while #(< % 4000000) (fibs))))
 )

(println (even_Fibs))

