(defn palindrome? [s]
  (= s (reverse s)))

(defn palindrome-number? [n]
  (palindrome? (seq (str n))))

(defn palindrome_product []
  (reduce max (filter palindrome-number?
                      (for [i (range 100 1000) j (range i 1000)] (* i j)))))
