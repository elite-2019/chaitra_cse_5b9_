(defn result [n]
      (if (zero? (mod res n))
        res
        (-> n largest-prime-factor (* res))))
    (reduce (range 2N 10001))

