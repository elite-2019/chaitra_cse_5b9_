dim = 8
board = []
directions = ['N', 'S', 'E', 'W', 'NE', 'NW', 'SE', 'SW']

def init ():
    for i in range(dim):
        board.append([])
        for j in range(dim):
            board[i].append(0)
    board[3][3] = 1
    board[3][4] = -1
    board[4][3] = -1
    board[4][4] = 1

def copy_board(b):
    newb = []
    for i in range(dim):
        newb.append([])
        for j in range(dim):
            newb[i].append(b[i][j])
    return newb
  
def dir_move (dir):
    if dir == 'N':
        return -1, 0
    elif dir == 'E':
        return 0, 1
    elif dir == 'SW':
        return 1, -1
    else:
        return 1, 1
def find_opp_dir (b, i, j, who, dir):
    di, dj = dir_move(dir)
    i1 = i + di
    j1 = j + dj
    tokens = 0
    if not (i1 < dim and j1 >= 0 and j1 < dim):
        return 0
    if not (b[i][j] == 0 and b[i1][j1] == who * -1):
        return 0
    
    while i1 >= 0 and i1 < dim and j1 >= 0 and j1 < dim:
        if b[i1][j1] == who * -1:
            
            i1 = i1 + di
            j1 = j1 + dj
            tokens += 1
        elif b[i1][j1] == who:
            
            return tokens
        else:
            
            return 0
   
    return 0

    

def reverse_line_dir (b, i, j, who, dir):
   
    return True


def score (b, who):
    result = 0
    for i in range(dim):
        for j in range(dim):
            if b[i][j] == who:
                result += 1
            elif b[i][j] == who * -1:
                result -= 1
    return result



def move (b, i, j, who):
    
    result = False
    if b[i][j] == 0:
        for dir in directions:
            
            if find_opp_dir(b, i, j, who, dir):
                result = True
                reverse_line_dir(b, i, j, who, dir)
    
    if result :
        b[i][j] = who
    return result


def next_move (b, who):
    
    return -1, -1



def print_board (state):
    print ("  0   1   2   3   4   5   6   7 ")
    print ("  +---+---+---+---+---+---+---+---+")
    c = 0
    for i in range(dim):
        print( i, "|",end=" ")
        for j in range(dim):
            c = state[i][j]
            if c == 0:
                print (" ",end=" ")
            elif c == 1:
                print ("*",end=" ")
            else:
                print ("O",end=" ")
            print( "|",end=" ")
        print( " " )
        print( "  +---+---+---+---+---+---+---+---+")



def play_me(who):
    
    i, j = next_move(board, who)
    if i >-1 and j>-1:
        print( "My next move is:", i, j)
        
        move(board, i, j, who)
    else:
        print( "I have no move, pass")
    print_board (board)
    print( "My score is:", score(board, who))


def play_other(i, j, who):
    if not move(board, i, j, who):
        print( "Your move cannot be done, pass")
    print_board (board)
    print( "Your score is:", score(board, who))


if __name__ == '__main__':
    
    init()
    print_board (board)
   
    print (move(board, 5, 3, 1))
    print (score(board, 1))
    print_board (board)
    
    print (move(board, 3, 5, 1))
    print_board (board)
