def Count(dice, number):
    return len([y for y in dice if y == number])

def HighestRepeated(dice, minRepeats):
    unique = set(dice)
    repeats = [x for x in unique if Count(dice, x) >= minRepeats]
    return max(repeats) if repeats else 0

def OfAKind(dice, n):
    return HighestRepeated(dice,n) * n

def SumOfSingle(dice, selected):
    return sum([x for x in dice if x == selected])

#strategies
def Chance(dice):
    return sum(dice)

def Pair(dice):
    return OfAKind(dice, 2)

def ThreeOfAKind(dice):
    return OfAKind(dice, 3)

def FourOfAKind(dice):
    return OfAKind(dice, 4)

def SmallStraight(dice):
    return 15 if tuple(sorted(dice)) == (1,2,3,4,5) else 0

def LargeStraight(dice):
    return 20 if tuple(sorted(dice)) == (2,3,4,5,6) else 0

def CountOnes(dice):
    return SumOfSingle(dice,1)

def CountTwos(dice):
    return SumOfSingle(dice,2)

def CountThrees(dice):
    return SumOfSingle(dice,3)

def CountFours(dice):
    return SumOfSingle(dice,4)

def CountFives(dice):
    return SumOfSingle(dice,5)

def CountSixes(dice):
    return SumOfSingle(dice,6)

def Yahtzee(dice):
    return 50 if len(dice) == 5 and len(set(dice)) == 1 else 0

def rollDice(n):
  rolls = []
  for i in range(n):
    rolls.append(random.randint(1,6))
  return rolls        
