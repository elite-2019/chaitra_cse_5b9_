def istrue(x):
    for i in range(0, n-1):
        if x[i] >= x[i+1]:
            return False
    return True

def reading1(s, odo_list):
    if s < max(odo_list):
        for i in range(0, len(odo_list)):
            if s < odo_list[i]:
                print("The next odometer list value is",odo_list[i])
                print("The previous odometer list value is",odo_list[i-2])
                return
    elif s == max(odo_list):
        print("The next odometer list value is",odo_list[0])
        print("The previous odometer list value is",odo_list[-2])
    elif s == min(odo_list):
        print("The next odometer list value is",odo_list[1])
        print("The previous odometer list value is",odo_list[-1])            

def x_moves1(y, step, odo_list):
    k = p = odo_list.index(y)
    print("The {} steps ahead are".format(step), end=" ")
    if (k+step) < len(odo_list):
        c = 0
        while c < step 1:
            print(odo_list[k+1], end=",")
            c += 1
            k += 1
        print(str(odo_list[p+step])+".")
        
    else:
        c = 1
        o = step
        while k + 1 < len(odo_list):
            print(odo_list[k+1],end=",")
            c += 1
            k += 1
        u = 0
        while c != o:
            print(odo_list[u],end=",")
            c += 1
            u += 1
            step -= 1
        print(str(odo_list[p+ o - len(odo_list)])+".")
        
def x_moves2(j, step, odo_list):
    k = p = odo_list.index(j)
    print("The {} steps behind are".format(step),end= " ")
    if (k - step ) >= 0:
        c = 0
        while c < step - 1:
            print(odo_list[k-1],end = ",")
            c += 1
            k -= 1
        print(str(odo_list[p-step])+".")
    else:
        c = 1
        o = step
        while k - 1 >= 0:
            print(odo_list[k-1],end=",")
            c += 1
            k -= 1
        u = len(odo_list)
        while c != o:
            print(odo_list[u-1],end=",")
            c += 1
            u -= 1
            step -= 1
        print(str(odo_list[u-1])+".")

n = int(input("Enter number of digits in the odometer in the range(2,8) both inclusive: "))
print()
odo_list = list(filter(lambda x : istrue(str(x)) ,range( pow(10, n-1) , pow(10, n))))
s = int(input("Enter {}-digit number ".format(n)))
print()
if len(str(s)) in range(2,9):
    if s in odo_list:
        reading1(s, odo_list)
        print()
        x = int(input("Enter number of steps to move ahead and behind "))
        print()
        x_moves1(s, x, odo_list)
        print()
        x_moves2(s, x, odo_list)
        print()
    else:
        print("{} is not present in the odometer list.".format(s))
        print()
else:
    print("Number of digits of this number is out of range.")
print()

