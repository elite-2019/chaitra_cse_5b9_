import time
t = time.time()

def matrix(x,n):
      l1 = []
      print("The puzzle is as following")
      for i in l:
          h = []
          for j in range(0, n):
              h.append(x[i+j])
          l1.append(h)
          print(" ".join(h))
      return l1
      
def commands():
    print("Give your command according to the movement of spaces:")
    print("A - to move the space upwards")
    print("B - to move the space downwards")
    print("R - to move the space rightwards")
    print("L - to move the space leftwards")
    print("O - to stop the moves")

def movement(m,l,n):
    for i in range(0,n):
        for j in range(0,n):
            if l[i][j] == str(' '):
                x = moves(l, i, j, m, n)
                return x

def moves(l1,t1,t2,m,n):

        if m == 'A' and t1-1 >= 0:
            l1[t1][t2] , l1[t1-1][t2] = l1[t1-1][t2] , l1[t1][t2]
        elif m == 'B' and t1+1 != n:
            l1[t1][t2] , l1[t1+1][t2] = l1[t1+1][t2] , l1[t1][t2]
        elif m == 'R' and t2+1 != n:
            l1[t1][t2] , l1[t1][t2+1] = l1[t1][t2+1] , l1[t1][t2]
        elif m == 'L' and t2-1 >= 0:
            l1[t1][t2] , l1[t1][t2-1] = l1[t1][t2-1] , l1[t1][t2]    
        else:
            print("Invalid entry")
        return l1


def print_list(l1):
    for s in l1:
        print(*s)
        
def print_moves(m,l,n):
    if m == 'O':
        print("Your final puzzle solved with 0 moves is ")
        print_list(l)
    else:
        while m != 'O':
            if m == 'A' or m =='B' or m == 'R' or m == 'L':
                l = movement(m,l,n)
                if type(l) == int:
                    print("Invalid Entry.Change your command")
                else:
                    print("Your puzzle's current status :")
                    print_list(l)
            else:
                print("Invalid entry.Change your command ")
            m = str(input("Enter your next command  "))
        print("Your final solved puzzle is:")
        print_list(l)
    return l

def c1(l):
    q = l[0][0]
    s = 0
    for i in range(0,n):
        for j in range(0,n):
            if i == 0 and j == 0 or i == n-1 and j == n-1:
                continue
            if ord(l[i][j]) < ord(q):
                break
            q = l[i][j]
            s += 1
    if s == n*n - 2:
        return True

n = int(input("Enter order of the square matrix.(For example - for a 3x3 grid, enter 3) : "))        
x = str(input("Enter elements with space included.(For example - a,b, ,c,b):")).split(",")
print()
l = list(filter(lambda x : x % n == 0,range(len(x))))
l = matrix(x,n)
if c1(l):
    print("Your puzzle is already solved.Try any other puzzle. :) ")
else:
    print()
    commands()
    print()
    m = str(input("Enter your command "))
    l = print_moves(m, l, n)
    if c1(l):
        print("Congratulations! You have solved the puzzle.")
        print_list(l)
print("You played for",round(time.time() - t),"seconds")
